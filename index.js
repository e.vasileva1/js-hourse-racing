const horseRaceGame = new HorseRacing();

function HorseRacing() {
    const PLAYER_DEFAULT_BANK = 1000;
    const MAX_TIME = 3000;
    const MIN_TIME = 500;

    this.horses = [
        // заполните список лошадей
        'Росинант',
        'Буцефал',
        'Кумир',
        'Кинго'
    ].map(name => new Horse(name));
    this.player = new Player(PLAYER_DEFAULT_BANK);
    this.wagers = [];
    this.clearWagers = function() {
        // очистить ставки
        this.wagers = [];
    };

    function Horse(name) {
        this.name = name;
        this.run = function() {
            // вернуть Promise, который будет автоматически выполнен через случайное время

            return new Promise(resolve => {
                const randomTime = Math.floor(Math.random() * (MAX_TIME - MIN_TIME) + MIN_TIME);

                setTimeout(() => resolve({
                    name: this.name, 
                    duration: randomTime,
                }), randomTime)
            });
        };
    }

    function Player(bank) {
        this.account = bank;
    }

    this.returnDefaultBank = function() {
        this.player.account = PLAYER_DEFAULT_BANK;
    }

    this.isHorseExist = function(name) {
        if (this.horses.find(horse => horse.name === name)) {
            return true;
        } else {
            console.error('Такая лошадь не зарегистрирована!');
            return false;
        }
    }

    this.isValidBet = function(sumToBet) {
        if (typeof sumToBet === 'number' && sumToBet > 0) {
            return checkAccount(sumToBet, this.player);
        }
        console.error('Неверный формат ставки!');
        return false;
    }

    function checkAccount(requiredAmount, player) {
        if (player.account >= requiredAmount) {
            return true;
        } else {
            console.error('У вас недостаточно средств!');
            return false;
        }
    }
}

function showHorses() {
    // вывести список лошадей
    console.log('Список лошадей:');
    horseRaceGame.horses.forEach(hourse => {
        console.log(hourse.name);
    });
}

function showAccount() {
    // показать сумму на счету игрока
    console.log('Счет игрока: ' + horseRaceGame.player.account);
}

function setWager(name, wager) {
    // сделать ставку на лошадь в следующем забеге
    if (horseRaceGame.isValidBet(wager) && horseRaceGame.isHorseExist(name)) {
        horseRaceGame.player.account -= wager;
        horseRaceGame.wagers.push({ name, wager });
        console.log('Ставка сделана!');
    }
}

function startRacing() {
    // начать забег

    const promises = horseRaceGame.horses.map(horse => 
        horse.run()
            .then(horse => {
                console.log(horse.name + ' - ' + horse.duration);
                return horse;
            })
        );

    let winner = null;
    Promise.race(promises).then((horse) => {
        winner = horse;
    });

    Promise.all(promises)
        .then(() => {
            horseRaceGame.wagers.forEach(horse => {
                if (horse.name == winner.name) {
                    horseRaceGame.player.account += horse.wager * 2;
                    console.log('Вы победили!');
                }
            });

            console.log('Ваш счет: ' + horseRaceGame.player.account);
            horseRaceGame.clearWagers();
        });

}

function newGame() {
    // восстановить исходный счет игрока
    console.clear();
    horseRaceGame.returnDefaultBank();
    horseRaceGame.clearWagers();
    console.log('Игра перезапущена');
}
